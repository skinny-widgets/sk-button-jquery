

import { SkButtonImpl }  from '../../sk-button/src/impl/sk-button-impl.js';


export class JquerySkButton extends SkButtonImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'button';
    }

    afterRendered() {
        super.afterRendered();
        if (this.buttonType) {
            this.button.classList.add(`sk-btn-${this.buttonType}`);
        }
        this.mountStyles();
    }

    restoreState(state) {
        super.restoreState(state);
        this.clearAllElCache();
        let disabled = this.comp.getAttribute('disabled');
        if (disabled) {
            this.buttonEl.setAttribute('disabled', 'disabled');
            this.buttonEl.classList.add('ui-state-disabled');
        }
    }

    enable() {
        super.enable();
        this.buttonEl.classList.remove('ui-state-disabled');
    }

    disable() {
        super.disable();
        this.buttonEl.classList.add('ui-state-disabled');
    }
}
